# System Architecture

A system architecture SVG for documentation purposes, etc.

![Interpeer System Architecture](./architecture.svg "Interpeer System Architecture")

* **Network Layer:** it's feasible/planned to create [packeteer](/interpeer/packeteer)
  connectors that operate directly on data link layer protocols.
* **Transport Layer:** [channeler](/interpeer/channeler) has its own addressing scheme;
  given a routing protocol, it could operate on the transport layer directly.
* **Session Layer:** mostly, *channeler* uses packeteer's socket API abstraction,
  in particular `AF_LOCAL` and UDP sockets.
  [Moldoveanu](/interpeer/moldoveanu) is intended to be an enhanced (and incompatible)
  version of Kademlia. [Hubur](/interpeer/hubur) is intended to be a streaming protocol
  operating at the *vessel extent* level of granularity.
* **Presentation Layer:** [CAProck](/interpeer/caprock) integrates with *vessel*, but also
  exposes an API upwards for managing distributed authentication. [vessel](/interpeer/vessel)
  is a streaming-optimized container file format with multi-authoring
  considerations built in. Finally, [wyrd](/interpeer/wyrd) is an API and plugin framework
  for representing files as a series of incremental modifications via a CRDT.
* **Application Layer:** The [GObject](/interpeer/gobject-sdk) and [Android](/interpeer/android-sdk)
  SDKs provide application developer APIs for the stack, with other SDKs feasible.

* **Layer Spanning:** [liberate](/interpeer/liberate) is a platform abstraction library
  used in multiple projects. Similarly, [s3kr1t](/interpeer/s3kr1t) encapsulates crypto
  libraries such that their respective APIs do not need to be known in the other
  code bases.

# Technology Tree

The technology tree below is a mixture of projects and larger features, so
an approximation of milestone based.
It is generated with [techtree](https://codeberg.org/librecast/techtree).

![Interpeer Technology Tree](./techtree.svg "Interpeer Technology Tree")

- *Salmon:* perpetually ongoing work; grows with the other tasks. This applies
  to e.g. documentation work, or base libraries.
- *Green:* completed, with only minor tweaks expected.
- *Teal:* initial implementation completed, but it will need more work for
  feature completeness.
- *Purple:* currently in progress.
- *Light gray:* planned, perhaps with a design outline, but not yet started.

Projects that have dependencies which are not yet done, but show more progress
are essentially vertical slices -- they have an API, but internally they need
to be adjusted once their dependencies come to life.
